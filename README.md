### 三勾多商户商城小程序、支持多端发布，一套代码发布到8个平台，面向开发，方便二次开发


### 项目介绍


三勾多商户小程序商城基于springboot+element-plus+uniapp打造的面向开发的小程序商城，方便二次开发或直接使用，可发布到多端，包括微信小程序、微信公众号、QQ小程序、支付宝小程序、字节跳动小程序、百度小程序、android端、ios端。


### 软件架构

- 后端：  springboot2.3.12
- 管理端页面：  element-plus vue3.0
- 移动端：  uniapp
- 数据库：  MySQL5.7

![输入图片说明](https://www.jjjshop.net/gitee/multi-java/jjjshop.png)
### 目录结构

- db：  数据库脚本
- jjj_shop_multi：  java端源码
- jjj_shop_multi_admin：  saas管理端vue页面
- jjj_shop_multi_shop：  shop商城后台vue页面
- jjj_shop_multi_supplier：  supplier商户端商城后台vue页面
- jjj_shop_multi_app：  移动端代码

### 技术特点
- 前后分离 (分工协助 开发效率高)
- 统一权限 (前后端一致的权限管理)
- uniapp (一套代码8个平台，开发不浪费)
- springboot (上手简单，极易开发)
- element-ui(饿了么前端开源管理后台框架，方便快速开发)

 ### 安装教程、开发文档、操作手册请进入官网查询

[官网链接](http://www.jjjshop.net)

[安装部署文档](https://doc.jjjshop.net/MultiJava)


### 项目演示 

- 官网地址：https://www.jjjshop.net/      

- shop端后台演示：https://demo-multi-java.jjjshop.net/shop     
账号密码1：admin/Jjjshop@123**[不可修改数据]** 
账号密码2：test/Jjjshop@123**[可修改数据]** 

- supplier端后台演示：https://demo-multi-java.jjjshop.net/supplier
账号密码1：admin/Jjjshop@123**[不可修改数据]** 
账号密码2：test/Jjjshop@123**[可修改数据]** 

- saas端演示：https://demo-multi-java.jjjshop.net/admin     账号密码：admin/Jjjshop@123


- 本地运行saas端和shop端账号密码均为    **admin/123456** 

- 本地运行移动端建议运行到浏览器，方便调试 账号密码 **18888888888/123456** 正式发布选择发布到微信小程序

 ### 扫码体验微信小程序，更多演示请扫码公众号查看 
![输入图片说明](https://www.jjjshop.net/gitee/multi-java/demo.png "demo.png")


 ### 如果对您有帮助，您可以点右上角 "Star" 支持一下，这样我们才有继续免费下去的动力，谢谢！
QQ交流群 (入群前，请在网页右上角点 "Star" )

交流QQ群：638926352  [点击加入](https://qm.qq.com/cgi-bin/qm/qr?k=qsJON0WddOXsEOov8zkLI4rTyEX_VDXA&jump_from=webapi)

 ### bug反馈

如果你发现了bug，请发送邮件到 bug@jiujiujia.net，我们将及时修复并更新。 

 ### 小程序截图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0713/115156_5c456fe7_1699189.jpeg "小程序截图-1.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0713/115205_538df117_1699189.jpeg "小程序截图-2.jpg")


 ### 后台截图 
![输入图片说明](https://images.gitee.com/uploads/images/2021/0713/134125_d2ae228c_1699189.jpeg "后台截图-1.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0713/134133_e80e6917_1699189.jpeg "后台截图-2.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0713/134142_5af2c033_1699189.jpeg "后台截图-3.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0713/134151_ed2b3a3c_1699189.jpeg "后台截图-4.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0713/134201_3b6e03dd_1699189.jpeg "后台截图-5.jpg")

 ### saas端截图 

![输入图片说明](https://images.gitee.com/uploads/images/2021/0702/105948_bb66da18_1699189.png "saas-1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0702/105956_ee6d1d73_1699189.png "saas-2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0702/110007_3f3b08c6_1699189.png "saas-3.png")
 